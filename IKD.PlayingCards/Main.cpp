// Playing Cards
// Izaac Dewilde
// Paul Haller

#include <iostream>
#include <conio.h>

using namespace std;

enum class Suit
{
	SPADES,
	DIAMONDS,
	HEARTS,
	CLUBS
};

enum class Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE,
}; 

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main()
{
	Card card1;
	card1.rank = Rank::KING;
	card1.suit = Suit::CLUBS;

	Card card2;
	card2.rank = Rank::JACK;
	card2.suit = Suit::HEARTS;

	PrintCard(HighCard(card1, card2));


	(void)_getch();
	return 0;
}

void PrintCard(Card card)
{
	string printSuit, printRank;
	switch (card.suit)
	{
	case Suit::SPADES:
		printSuit = "Spades";
		break;
	case Suit::DIAMONDS:
		printSuit = "Diamonds";
		break;
	case Suit::HEARTS:
		printSuit = "Hearts";
		break;
	case Suit::CLUBS:
		printSuit = "Clubs";
		break;
	default:
		cout << "Invalid Suit";
		break;
	}

	switch (card.rank)
	{
	case Rank::TWO:
		printRank = "Two";
		break;
	case Rank::THREE:
		printRank = "Three";
		break;
	case Rank::FOUR:
		printRank = "Four";
		break;
	case Rank::FIVE:
		printRank = "Five";
		break;
	case Rank::SIX:
		printRank = "Six";
		break;
	case Rank::SEVEN:
		printRank = "Seven";
		break;
	case Rank::EIGHT:
		printRank = "Eight";
		break;
	case Rank::NINE:
		printRank = "Nine";
		break;
	case Rank::JACK:
		printRank = "Jack";
		break;
	case Rank::QUEEN:
		printRank = "Queen";
		break;
	case Rank::KING:
		printRank = "King";
		break;
	case Rank::ACE:
		printRank = "Ace";
		break;
	default:
		cout << "Invalid Rank";
		break;
	}

	cout << "The " << printRank << " of " << printSuit;

}

Card HighCard(Card card1, Card card2)
{
	if (card1.suit > card2.suit)
		return card1;
	else if (card2.suit > card1.suit)
		return card2;
	else if (card1.suit == card2.suit)
	{
		if (card1.rank > card2.rank)
			return card1;
		else
			return card2;
	}
}